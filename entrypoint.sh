#!/bin/sh

set -e

#Use a substitute to pass the env variables to the config file
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

#Run nginx service with daemon off (run in the foreground)
nginx -g 'daemon off;'