
# Set the image and the maintainer
FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="maintainer@londonappdev.com"

# Copy the config files we cretaed into the container
COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

# Configure the Env variables
ENV LISTEN_PORT=8000
ENV APP_HOST=app 
ENV APP_PORT=9000

# Switch to root user in order to make some changes that needs privileges
USER root

# Set config directories and permissions
RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

# Copy the entrypoint file and make it executable
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Switch to NGINX user
USER nginx

# Set default command to run the dockerfile or image inside the container
CMD ["/entrypoint.sh"]